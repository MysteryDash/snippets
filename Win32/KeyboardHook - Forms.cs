﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.0.
// The full license text can be found in the file named LICENSE.
// Written originally by Alexandre Quoniou in 2016.
// 

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MysteryDash.Win32
{
    /// <summary>
    ///     A static class providing a global keyboard hook.
    /// </summary>
    /// <remarks>
    ///     This class has been made for use without Windows Forms or Windows Presentation Foundation.
    /// </remarks>
    public static class KeyboardHook
    {
        private const int WhKeyboardLl = 13;
        private const int WmKeyDown = 0x0100;
        private const int WmKeyUp = 0x0101;
        private const int WmSysKeyDown = 0x0104;
        private const int WmSysKeyUp = 0x0105;

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        private static readonly LowLevelKeyboardProc hookCallback = HookCallback;
        private static IntPtr _hookId = IntPtr.Zero;
        private static HashSet<Keys> _keysDown;

        public static bool Enabled => _hookId != IntPtr.Zero;

        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        /// <summary>
        ///     Event raised when a key is pressed.
        /// </summary>
        /// <remarks>
        ///     This event will only be raised once per keystroke.
        /// </remarks>
        public static event EventHandler<KeyEventArgs> KeyPressed;

        /// <summary>
        ///     Event raised when a key is pressed.
        /// </summary>
        /// <remarks>
        ///     This event can be raised multiple times by a single keystroke.
        /// </remarks>
        public static event EventHandler<KeyEventArgs> KeyDown;

        /// <summary>
        ///     Event raised when a key released.
        /// </summary>
        public static event EventHandler<KeyEventArgs> KeyUp;

        /// <summary>
        ///     Starts listening for any keyboard input.
        /// </summary>
        public static void Hook()
        {
            if (_hookId == IntPtr.Zero)
            {
                _keysDown = new HashSet<Keys>();

                using (var process = Process.GetCurrentProcess())
                using (var module = process.MainModule)
                {
                    _hookId = SetWindowsHookEx(WhKeyboardLl, hookCallback, GetModuleHandle(module.ModuleName), 0);
                }
            }
        }

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                var key = (Keys)Marshal.ReadInt32(lParam);

                if (wParam == (IntPtr)WmKeyUp || wParam == (IntPtr)WmSysKeyUp)
                {
                    _keysDown.Remove(key);
                    KeyUp?.Invoke(null, new KeyEventArgs(key));
                }

                if (wParam == (IntPtr)WmKeyDown || wParam == (IntPtr)WmSysKeyDown)
                {
                    if (!_keysDown.Contains(key))
                    {
                        KeyPressed?.Invoke(null, new KeyEventArgs(key));
                    }

                    _keysDown.Add(key);
                    KeyDown?.Invoke(null, new KeyEventArgs(key));
                }
            }

            return CallNextHookEx(_hookId, nCode, wParam, lParam);
        }

        /// <summary>
        ///     Stops the hook and unsubscribe any registered event only if the hook is already enabled.
        /// </summary>
        public static void Unhook()
        {
            if (_hookId != IntPtr.Zero)
            {
                UnhookWindowsHookEx(_hookId);

                foreach (EventHandler<KeyEventArgs> d in KeyPressed?.GetInvocationList() ?? new Delegate[0])
                {
                    KeyPressed -= d;
                }
                foreach (EventHandler<KeyEventArgs> d in KeyUp?.GetInvocationList() ?? new Delegate[0])
                {
                    KeyUp -= d;
                }
                foreach (EventHandler<KeyEventArgs> d in KeyDown?.GetInvocationList() ?? new Delegate[0])
                {
                    KeyDown -= d;
                }
            }
        }
    }
}