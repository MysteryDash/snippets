﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.0.
// The full license text can be found in the file named LICENSE.
// Written originally by Alexandre Quoniou in 2016.
// 

using System;

namespace MysteryDash.Cryptography
{
    /// <summary>
    ///     A static class providing methods to crypt or decrypt a string using caesar cipher.
    /// </summary>
    /// <remarks>
    ///     This class will only affect the characters of the alphabet but it'll keep the case.
    /// </remarks>
    public static class CaesarCipher
    {
        private const string UpperCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string LowerCharset = "abcdefghijklmnopqrstuvwxyz";

        public static string Crypt(char[] cipher, int shift)
        {
            if (cipher == null)
            {
                throw new ArgumentNullException(nameof(cipher));
            }

            for (int i = 0; i < cipher.Length; i++)
            {
                var lowerIndex = LowerCharset.IndexOf(cipher[i]);
                if (lowerIndex != -1)
                {
                    cipher[i] = LowerCharset[(lowerIndex + shift) % LowerCharset.Length];
                }
                else
                {
                    var upperIndex = UpperCharset.IndexOf(cipher[i]);
                    if (upperIndex != -1)
                    {
                        cipher[i] = UpperCharset[(upperIndex + shift) % UpperCharset.Length];
                    }
                }

            }

            return new string(cipher);
        }

        public static string Decrypt(char[] cipher, int shift)
        {
            return Crypt(cipher, -shift);
        }
    }
}